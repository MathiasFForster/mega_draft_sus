"uses strict";

let all_projects_arr = [];
let regex_projId = new RegExp('^[a-zA-Z][a-zA-Z0-9/&_]{2,10}$');
let regex_ownerN = new RegExp('^[a-zA-Z][a-zA-Z0-9/]{2,10}$');
let regex_ProjT = new RegExp('[a-zA-Z0-9]{3,25}$');
let regex_NberAndRate = new RegExp('^[0-9]{1,3}$');
let regex_ProjDesc = new RegExp('^[a-zA-Z ]{3,65}$');
let validations = [true,true,true,true,true,true,true,true];
let status_bar;

document.addEventListener('DOMContentLoaded', function() {

    enable_disable_Button(true);

    let project_id = document.getElementById('project_id');
    project_id.addEventListener("blur", validateProjId);

    let owner_name = document.getElementById('owner_name');
    owner_name.addEventListener("blur", validateOwnerName);

    let title = document.getElementById('title');
    title.addEventListener("blur", validateTitle);

    let hours = document.getElementById('hours');
    hours.addEventListener("blur", validateHours);

    let rate = document.getElementById('rate'); 
    rate.addEventListener("blur", validateRate);

    let description = document.getElementById('s-desc');
    description.addEventListener("blur", validateDesc);

    let category = document.getElementById('category');
    category.addEventListener("blur", validateCategory);
    
    let status = document.getElementById('status');
    status.addEventListener("blur", validateStatus);
    
    let button_add = document.getElementsByTagName('button')[0];
    button_add.addEventListener("click", generate_table);

    let queryFilter = document.getElementById('query');
    queryFilter.addEventListener("blur", filter);
    
    checkIfTableNull();

    let saveButton = document.getElementById('save');
    saveButton.addEventListener("click", saveAllProjects2Storage);

    let loadButton = document.getElementById("load");
    loadButton.addEventListener("click", readAllProjectsFromStorage);

    let clearButton = document.getElementById("clear");
    clearButton.addEventListener("click", clearAllProjectsFromStorage);

    let appendButton = document.getElementById("append");
    appendButton.addEventListener("click", appendAllProjects2Storage);

    status_bar = document.getElementById('status_bar');

    document.getElementsByName('sort')[0].addEventListener("click", function(){
        if(all_projects_arr != null)
            sorting_activation(event);
    });
    document.getElementsByName('sort')[1].addEventListener("click", function(){
        if(all_projects_arr.length != null)
            sorting_activation(event);
    });

    let column_selection = document.getElementById('column');
    column_selection.addEventListener("blur", sorting_activation);
});

function validateRate() {

    repetition_avoidance('sixth-div', 5);

    setElementFeedback('rate',validateElement(regex_NberAndRate,getRate()), 'sixth-div');

    validations_insertion(regex_NberAndRate, getRate(), 0);

    button_validation();
}
function validateDesc() {

    repetition_avoidance('short-description', 7);

    setElementFeedback('s-desc',validateElement(regex_ProjDesc,getDescription()), 'short-description');

    validations_insertion(regex_ProjDesc,getDescription(), 1);

    button_validation();
}
function validateTitle() {

    repetition_avoidance('third-div', 2);

    setElementFeedback('title',validateElement(regex_ProjT,getTitle()), 'third-div');

    validations_insertion(regex_ProjT, getTitle(), 2);

    button_validation(); 
}
function validateHours() {

    repetition_avoidance('fifth-div', 4);

    setElementFeedback('hours',validateElement(regex_NberAndRate,getHours()), 'fifth-div');

    validations_insertion(regex_NberAndRate, getHours(), 3);

    button_validation();
}
function validateOwnerName() {

    repetition_avoidance('second-div', 1);

    setElementFeedback('owner_name',validateElement(regex_ownerN, getOwner_Name()), 'second-div');

    validations_insertion(regex_ownerN,getOwner_Name(), 4);

    button_validation();
}
function validateProjId() {

    repetition_avoidance('first-div', 0);

    setElementFeedback('project_id',validateElement(regex_projId,getProject_Id()), 'first-div');

    validations_insertion(regex_projId, getProject_Id(), 5);
    
    button_validation();
}

function validateCategory() {

    repetition_avoidance_for_lists('fourth-div', 3);

    if(getCategory() == 'practical' || getCategory() == 'theoretical' || getCategory() == 'fundamental research' || getCategory() == 'empirical') {
        setElementFeedback('category', true, 'fourth-div');
        validations[6] = false;
    } else {
        setElementFeedback('category', false, 'fourth-div');
        validations[6] = true;
    }
    button_validation();
}
function validateStatus() {

    repetition_avoidance_for_lists('seventh-div', 6);

    if(getStatus() == 'completed' || getStatus() == 'ongoing' || getStatus() == 'planned') {
        setElementFeedback('status', true, 'seventh-div');
        validations[7] = false;
    } else {
        setElementFeedback('status', false, 'seventh-div');
        validations[7] = true;
    }

    button_validation();
}
function button_validation() {

    enable_disable_Button(false);

    for(let i = 0; i < validations.length; i++) {

        if(validations[i]) {
            enable_disable_Button(true);
            break;
        }
    }
}

function generate_table() {

    updateProjectsTable();
}
function filter() { 

    filterProjects(getFilter());
}
 

