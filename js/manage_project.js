/**
 * Creates an individual project from the inputs in the 
 * required fields.
 * @returns {Object} - Returns a project as an object
 * 
 * Mathias
 */
function createProjectObject() {

    let project = new Object();
    project.projectId = getProject_Id();
    project.ownerName = getOwner_Name();
    project.title = getTitle();
    project.category = getCategory();
    project.status = getStatus();
    project.hours = getHours();
    project.rate = getRate();
    project.description = getDescription();

    return project;
}

/**
 * Deletes a single row from the table of projects
 * @param {Object} event - From the eventListener
 * 
 * Michael
 */
function deleteTableRow(event) {

    let row = event.target.parentNode.parentNode;
    let projId = row.childNodes[0].textContent;
    
    let deletion = "Do you want to delete project " + projId + "?";
    if(confirm(deletion) == true) {
        for(let i = 0; i < all_projects_arr.length; i++){
            if(all_projects_arr[i]['projectId'] == projId)
                all_projects_arr.splice(i, 1);
        }

        let tbody = document.getElementById('table_body');
        tbody.innerHTML = '';

        createTableFromArrayObjects(all_projects_arr, 'table_body');
        status_bar.innerText = "Project " + projId + " succesfully deleted ...";
        
    }
}

/**
 * Changes all fields in a row to inputs to edit values.
 * Also changes edit button to a save button to save the values when clicked.
 * @param {Object} event - From the eventListener
 * 
 * Michael
 */
function editTableRow(event){

    let row = event.target.parentNode.parentNode;
    let innerText;
    let projId = row.childNodes[0].innerText;
    for(let i = 0; i < row.childNodes.length-2; i++){
        innerText = row.childNodes[i].innerText;
        row.childNodes[i].innerHTML = '<input type="text" value="' + innerText + '">';
    }
    row.childNodes[8].innerHTML = '';
    let save = document.createElement('img');
    save.src = '../images/save.png';
    save.addEventListener("click", function(event){
        let row = event.target.parentNode.parentNode;

        for(let i = 0; i < all_projects_arr.length; i++){
            if(all_projects_arr[i]['projectId'] == projId){
                all_projects_arr[i].ownerName = row.childNodes[1].childNodes[0].value;
                all_projects_arr[i].title = row.childNodes[2].childNodes[0].value;
                all_projects_arr[i].category = row.childNodes[3].childNodes[0].value;
                all_projects_arr[i].status = row.childNodes[4].childNodes[0].value;
                all_projects_arr[i].hours = row.childNodes[5].childNodes[0].value;
                all_projects_arr[i].rate = row.childNodes[6].childNodes[0].value;
                all_projects_arr[i].description = row.childNodes[7].childNodes[0].value;

                document.getElementById('table_body').innerHTML = '';
                createTableFromArrayObjects(all_projects_arr, 'table_body');
                status_bar.innerText = "New values for project " + projId + " saved ...";
            }
        }
    });
    row.childNodes[8].appendChild(save);

}

/**
 * Displays all projects matching the user's search in any field.
 * @param {String} pattern 
 * 
 * Michael
 */
function filterProjects(pattern){

    let result_array = all_projects_arr.filter(project => {
        for(key in project){
            if(project[key].includes(pattern))
                return true;
        }
        return false;
    });
    status_bar.innerText = "There is/are " + result_array.length + " project(s) for your query ...";

    let tbody = document.getElementById('table_body');
    tbody.innerHTML = '';

    createTableFromArrayObjects(result_array, "table_body");
}

/**
 * Adds a project to the table based on the inputs from the fieldset.
 * 
 * Michael
 */
function updateProjectsTable() {

    all_projects_arr.push(createProjectObject());

    let tbody = document.getElementById('table_body');
    tbody.innerHTML = '';

    createTableFromArrayObjects(all_projects_arr, 'table_body');

    status_bar.innerText = "Project added succesfully ...";
}

/**
 * Saves all current projects to local storage.
 * Also overrides existing save in local storage.
 * 
 * Michael
 */
function saveAllProjects2Storage(){
    localStorage.setItem('all_projects_arr', JSON.stringify(all_projects_arr));
    status_bar.innerText = "Current project(s) saved to local storage succesfully ...";
}

function appendAllProjects2Storage(){
    let storedProjectArr = JSON.parse(localStorage.getItem('all_projects_arr'));

    if(storedProjectArr != null && all_projects_arr.length != 0){
        for(let project of all_projects_arr){
            storedProjectArr.push(project);
        }
        all_projects_arr = storedProjectArr;
        saveAllProjects2Storage();
        status_bar.innerText = "Current project(s) appended to local storage succesfully ...";
    }
}

/**
 * Loads all projects from local storage.
 * 
 * Michael
 */
function readAllProjectsFromStorage(){
    
    let storedArr = JSON.parse(localStorage.getItem('all_projects_arr'));

    if(storedArr != null){
        all_projects_arr = storedArr;
        document.getElementById('table_body').innerHTML = '';
        createTableFromArrayObjects(all_projects_arr, 'table_body');
        status_bar.innerText = "Project(s) from local storage loaded ...";
    }
    else {
        status_bar.innerText = "No projects currently in storage ...";
    }
}

/**
 * Activates sorting in either ascending or descending order
 * and bases order on the column chose by the user.
 * @param {Object} event - From eventListener 
 * 
 * Michael
 */
function sorting_activation(event){
    
    let fieldset = event.target.parentNode;
    let column = fieldset.children[2].value;
    let sortedArr;
    
    if(fieldset.children[3].checked){
        sortedArr = sortAsc(column);
    }
    else if(fieldset.children[5].checked){
        sortedArr = sortDesc(column);
    }

    document.getElementById('table_body').innerHTML = '';
    createTableFromArrayObjects(sortedArr, 'table_body');

    status_bar.innerText = "Projects sorted by " + column +" ...";
}

/**
 * Sorts the projects in ascending order based on the column
 * @param {String} column - Represents the column to order by 
 * @returns Array of sorted projects
 * 
 * Michael
 */
function sortAsc(column){
    let sortedArr = all_projects_arr;
    let min;
    let minIndex;
    for(let i = 0; i < sortedArr.length; i++){
        min = sortedArr[i];
        minIndex = i;
        for(let j = i; j < sortedArr.length; j++){
            if(min[column] > sortedArr[j][column]){
                min = all_projects_arr[j];
                minIndex = j;
            }
        }
        sortedArr = swapPositions(minIndex, i, sortedArr);
    }

    return sortedArr;
}

/**
 * Sorts the projects in descending order based on the column
 * @param {String} column 
 * @returns Array of sorted projects
 * 
 * Michael
 */
function sortDesc(column){
    let sortedArr = all_projects_arr;
    let max;
    let maxIndex;
    for(let i = 0; i < sortedArr.length; i++){
        max = sortedArr[i];
        maxIndex = i;
        for(let j = i; j < sortedArr.length; j++){
            if(max[column] < sortedArr[j][column]){
                max = sortedArr[j];
                maxIndex = j;
            }
        }
        sortedArr = swapPositions(maxIndex, i, sortedArr);
    }
    return sortedArr;
}

/**
 * Clears the local storage
 */
function clearAllProjectsFromStorage(){
    localStorage.clear();
    status_bar.innerText = "Local storage cleared ...";
}

/**
 * All of the following functions get the user's input from the fieldset
 * @returns {String} value of user input
 * 
 * Mathias
 */
function getProject_Id() {
    let p_id = document.getElementById('project_id').value;
    return p_id;
}
function getOwner_Name() {
    let o_name = document.getElementById('owner_name').value;
    return o_name;
}
function getTitle() {
    let title = document.getElementById('title').value;
    return title;
}
function getCategory() {
    let category = document.getElementById('category').value;
    return category;
}
function getHours() {
    let hours = document.getElementById('hours').value;
    return hours;
}
function getRate() {
    let rate = document.getElementById('rate').value;
    return rate;
}
function getStatus() {
    let status = document.getElementById('status').value;
    return status;
}
function getDescription() {
    let description = document.getElementById('s-desc').value;
    return description;
}
function getFilter() {
    let filter = document.getElementById('query').value;
    return filter;
}